package com.cheypnow.Game;

import com.cheypnow.Graphics.Renderer;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

public class Game extends Canvas {

    private static final long serialVersionUID = 1;

    private static int width = 300;
    private static int height = width / 16 * 9;
    private static int scale = 3;

    private Thread thread;
    private boolean running = false;

    private BufferStrategy buffer = null;
    private BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
    private com.cheypnow.Graphics.Renderer renderer;

    private String title = "Rage game";
    private JFrame frame = new JFrame(title);


    public Game() {
        Dimension size = new Dimension(width * scale, height * scale);
        setPreferredSize(size);
        renderer = new Renderer(width, height, pixels);
    }

    private synchronized void Start() {
        init();
        running = true;
        thread = new Thread(() -> {

            long timer = System.currentTimeMillis();
            long jvmLastTime = System.nanoTime();

            double jvmPartTime = 1_000_000_000.0 / 60.0;
            double delta = 0;

            int updates = 0;
            int frames = 0;


            while (running) {

                long jvmNom = System.nanoTime();
                delta += jvmNom - jvmLastTime;
                jvmLastTime = jvmNom;

                if (delta >= jvmPartTime) {
                    update();
                    updates++;
                    delta = 0;
                }

                render();
                frames++;

                if (System.currentTimeMillis() - timer > 1000) {
                    timer += 1000;
                    frame.setTitle(title + " | " + " Updates: " + updates + " | " + "Frames: " + frames + " |");
                    updates = 0;
                    frames = 0;
                }
            }
        });
        thread.start();
    }

    private synchronized void Stop() {
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    private void update() {
    }

    private void render() {

        if (buffer == null) {
            createBufferStrategy(3);
            buffer = getBufferStrategy();
        }
        renderer.clear();
        renderer.render();
        Graphics g = buffer.getDrawGraphics();
        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
        g.dispose();
        buffer.show();
    }

    private void init() {
        frame.setResizable(false);
        frame.setTitle("Fury");
        frame.add(this);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    public static void main(String[] args) {

        new Game().Start();

    }
}
